//
//  ViewController.swift
//  Project2
//
//  Created by Роман Хоменко on 27.03.2022.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet var firstFlagButton: UIButton!
    @IBOutlet var secondFlagButton: UIButton!
    @IBOutlet var thirdFlagButton: UIButton!
    
    var countries = [String]()
    var score = 0
    var highestScore = 0
    var correctAnswer = 0
    
    var questionCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showScoresTapped))
        
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "us", "uk"]
        
        firstFlagButton.layer.borderWidth = 1
        secondFlagButton.layer.borderWidth = 1
        thirdFlagButton.layer.borderWidth = 1
        
        firstFlagButton.layer.borderColor = UIColor.lightGray.cgColor
        secondFlagButton.layer.borderColor = UIColor.lightGray.cgColor
        thirdFlagButton.layer.borderColor = UIColor.lightGray.cgColor
        
        let defaults = UserDefaults.standard
        highestScore = defaults.integer(forKey: "highestScore")
        print(highestScore)
        
        askQuestion()
        // alert with ask of permission to notificate user
        registerLocal()
        registerNotification()
    }
    
    func askQuestion(action: UIAlertAction! = nil) {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        
        firstFlagButton.setImage(UIImage(named: countries[0]), for: .normal)
        secondFlagButton.setImage(UIImage(named: countries[1]), for: .normal)
        thirdFlagButton.setImage(UIImage(named: countries[2]), for: .normal)
        
        title = "\(countries[correctAnswer].uppercased()), score: \(score), highest score: \(highestScore)"
    }
    
    // MARK: - IBActions
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: []) {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        } completion: { _ in
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: []) {
                sender.transform = .identity
            } completion: { _ in
                var title: String
                
                if sender.tag == self.correctAnswer {
                    title = "Correct"
                    self.score += 1
                } else {
                    title = "Wrong, this is \(self.countries[sender.tag])'s flag"
                    self.score -= 1
                }
                
                self.questionCounter += 1
                
                if self.questionCounter == 5 {
                    if self.score > self.highestScore {
                        self.highestScore = self.score
                        self.save()
                        
                        let alert = UIAlertController(title: "Your new highest score is \(self.score)", message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Thanks!", style: .default))
                        self.present(alert, animated: true)
                    }
                    
                    let alert = UIAlertController(title: title, message: "Your final score is \(self.score)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Reset", style: .default))
                    self.present(alert, animated: true)
                    
                    self.questionCounter = 0
                    self.score = 0
                }
                
                self.askQuestion()
            }
        }
    }
    
    @objc func showScoresTapped() {
        let alert = UIAlertController(title: "Scores", message: "Your score is \(score)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Continue", style: .default))
        present(alert, animated: true)
    }
    
}

extension ViewController {
    func save() {
        let defaults = UserDefaults.standard
        defaults.set(highestScore, forKey: "highestScore")
    }
}

// MARK: - Setting UserNotification for player
extension ViewController {
    func registerLocal() {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert]) { granted, error in
            if granted {
                print("Get acces to notifications")
            } else {
                print("Reject notifications")
            }
        }
    }
    
    func registerNotification() {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        let content = UNMutableNotificationContent()
        content.title = "Flag's every days reminder is here!"
        content.body = "Go and try again!"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 86400,
                                                        repeats: true)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString,
                                            content: content,
                                            trigger: trigger)
        
        center.add(request)
    }
}
